# **欢迎有想开发的一起来完善该网站，请联系QQ947796393**

# **Javashare项目简介**
  旨在为Java爱好者提供一个免费学习的地方，包括主流培训机构视频教程，高清PDF电子书，技术说明文档，高质量项目源代码等等服务。
  
# **技术选型**
- 核心框架：**sprigboot**
- 前端框架：**thymeleaf**

# **UI效果图**
![输入图片说明](https://images.gitee.com/uploads/images/2018/0927/171428_3d214593_530047.png "Java知识分享网-首页.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0927/171500_886145ee_530047.png "Java知识分享网-搜索页（无结果）.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0927/171904_76b16296_530047.png "搜索-min.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0927/171513_8b63b12b_530047.png "Java知识分享网-文章详情页.png")

![输入图片说明](https://images.gitee.com/uploads/images/2018/0927/174224_535bba81_530047.png "Java知识分享网-分享页面.png")

