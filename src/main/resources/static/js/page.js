let page = 1;                   // 当前页数，初始值设为 1
let size = 10;                  // 每页条数，初始值设为 10
let total;                      // 总记录数


function getInfoToPage() {
    getInfo();  // 获取数据
    toPage();   // 进行分页
}

function getInfo() {
    var url = document.URL;
    var tag = "";
    if (url.indexOf("list") == -1) {
        // 需要渲染页面及数据
        var keys = $('#keyboard').val();
        window.location.href ="list?keys="+keys;

    } else{
        tag = "list_data";
    }

    $.ajax({
        type: "post",
        url: tag,
        async: false,
        dataType: 'json',
        data: {
            "keys": $('#keyboard').val() == '搜索你喜欢的' ? GetQueryString("keys"): $('#keyboard').val() ,
            "page": page,
            "size": size,
        },
        success: (data) => {
        if(data.code == 400){
        $("#list").html("<h2 style='color: red;text-align: center'>查无数据！</h2>");
        return;
    }
    // 1.清空原数据
    $("#list").html("");

    // 2.重新赋值页码、条数、总记录数
    page = data.page;
    size = data.size;
    total = data.total;

    // 3.渲染数据
    let text = ``;
    $.each(data.list, (i, item) => {
        text +=`
                     <li>
                        <h2><a href="/">${item.title}</a></h2>
                        <i><a href="/"><img
                                src="http://www.yangqq.com/d/file/jstt/bj/2018-06-29/3f0b6da48a6fd4e626a021ff7bd0d74f.jpg"></a></i>
                        <p class="blogtext">
                            难度：0基础</br>
                            知识点：HTML基础、标签、CSS 选择器、盒子模型、浮动、定位切图等页面相关常用技术</br>
                            建议学习周期：<b>5</b>天
                        </p>
                        <p class="bloginfo"><span>尚硅谷</span><span>2018年3月19日</span><span><a href="/">Java基础视频</a></span>
                        </p>
                    </li>
            `;
});
    $("#list").html(text);
}
})
};

// 分页脚本
function toPage() {
    layui.use('laypage', function () {
        let laypage = layui.laypage;
        // 调用分页
        laypage.render({
            // 分页容器的id
            elem: 'page',// *必选参数
            // 数据总数，从服务端得到
            count: total,// *必选参数
            // 每页显示的条数。laypage将会借助 count 和 limit 计算出分页数。
            //limit:limit,
            // 起始页
            //curr:page,
            // 连续出现的页码个数
            //groups:5,
            // 自定义每页条数的选择项
            // 自定义首页、尾页、上一页、下一页文本
            first: '首页',
            last: '尾页',
            prev: '<em><<</em>',
            next: '<em>>></em>',
            // 自定义主题
            theme: "#09b1b9",
            // 自定义排版
            layout: ['count', 'prev', 'page', 'next'],
            // 渲染数据
            jump: function (data, first) {
                // data包含了当前分页的所有参数
                page = data.curr;
                size = data.limit;
                getInfo();
            }
        });
    })
}

function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)
        return unescape(r[2]);
    return null;
}