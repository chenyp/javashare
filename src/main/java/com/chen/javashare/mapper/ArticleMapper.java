package com.chen.javashare.mapper;

import com.chen.javashare.vo.ArticleVo;
import com.chen.javashare.vo.SearchVo;

import java.util.List;
import java.util.Map;

public interface ArticleMapper {

    public List<SearchVo> articleList(Map<String,Object> paems);
}
