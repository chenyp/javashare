package com.chen.javashare.service.impl;

import com.chen.javashare.mapper.ArticleMapper;
import com.chen.javashare.service.IArticleService;
import com.chen.javashare.vo.ArticleVo;
import com.chen.javashare.vo.Page;
import com.chen.javashare.vo.SearchVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ArticleServiceImpl implements IArticleService {

    @Autowired
    private ArticleMapper articleMapper;

    @Override
    public Page<SearchVo> list(int type, int pageNum, String keyWord) {


        if (pageNum <= 0){
            pageNum = 1;
        }

        int rows = 10;
        int offset = (pageNum-1)*10;
        Map<String,Object> parms = new HashMap<>();
        parms.put("keyword",keyWord);
        PageHelper.offsetPage(offset,rows);
        List<SearchVo> list = articleMapper.articleList(parms);
        PageInfo<SearchVo> pageInfo = new PageInfo<>(list);

        Page<SearchVo> reslut = new Page<SearchVo>(pageNum,rows,(int)pageInfo.getTotal(),list);
        return reslut;
    }
}
