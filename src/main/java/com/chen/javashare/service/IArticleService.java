package com.chen.javashare.service;

import com.chen.javashare.vo.Page;
import com.chen.javashare.vo.SearchVo;

import java.util.List;

public interface IArticleService {
    Page<SearchVo> list(int type, int pageNum, String keyWord);
}
