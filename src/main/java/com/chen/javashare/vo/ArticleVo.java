package com.chen.javashare.vo;

import com.chen.javashare.entity.Article;
import com.chen.javashare.entity.Category;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

public class ArticleVo implements Serializable {
    public Category category;

    public List<Article> articles;

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }
}
