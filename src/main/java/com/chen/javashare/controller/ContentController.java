package com.chen.javashare.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class ContentController {

    @RequestMapping("content/{contentId}")
    public String contentInfo(@PathVariable("contentId") Integer contentId, Model model){
        System.out.println("获取到的参数："+contentId);
        model.addAttribute("contitle","文章标题123");
        model.addAttribute("author","文章作者123");
        model.addAttribute("createdDate","创建时间123");
        model.addAttribute("type","Java123");
        return "info";
    }
}
