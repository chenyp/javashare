package com.chen.javashare.controller;

import com.chen.javashare.service.IArticleService;
import com.chen.javashare.vo.ArticleVo;
import com.chen.javashare.vo.Page;
import com.chen.javashare.vo.SearchVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class SearchController {

    @Autowired
    private IArticleService articleService;

    @RequestMapping("list_data")
    @ResponseBody
    public Map<String,Object> list(@RequestParam("keys") String keys,
                                   @RequestParam("page") Integer page,
                                   @RequestParam("size") Integer size){
        Map<String,Object> map = new HashMap<>();

        Page<SearchVo> result = articleService.list(0,page,keys);
        if (result.getList().size() == 0){
            map.put("code",400);
            return map;
        }
        map.put("total",result.getTotal());
        map.put("list",result.getList());
        map.put("code",100);
        map.put("page",result.getPage());
        map.put("size",result.getSize());
        return map;
    }

    @RequestMapping("lists_data")
    public ModelAndView lists(@RequestParam("keys") String keys,
                                   @RequestParam("page") Integer page,
                                   @RequestParam("size") Integer size){
        ModelAndView modelAndView = new ModelAndView();

        Page<SearchVo> result = articleService.list(0,page,keys);
        if (result.getList().size() == 0){
            modelAndView.addObject("code",400);
            return modelAndView;
        }
        modelAndView.addObject("total",result.getTotal());
        modelAndView.addObject("list",result.getList());
        modelAndView.addObject("code",100);
        modelAndView.addObject("page",result.getPage());
        modelAndView.addObject("size",result.getSize());
        modelAndView.setViewName("redirect:list");
        return modelAndView;
    }

    public static List<SearchVo> listData(){
        List<SearchVo> list = new ArrayList<>();
        for (int i = 0; i<10;i++){
            SearchVo searchVo = new SearchVo();
            searchVo.setTitle("第"+i+"个教程-----------》");
            searchVo.setAuthor("admin");
            searchVo.setCycle(20+"");
            searchVo.setPoint("HTML基础、标签、CSS 选择器、盒子模型、浮动、定位切图等页面相关常用技术");
            list.add(searchVo);
        }
        return list;
    }

    public ModelAndView search(){
        ModelAndView modelAndView = new ModelAndView();
        return modelAndView;
    }

}
