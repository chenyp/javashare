package com.chen.javashare.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PageController {

    /**
     * 学习路线图
     * @return
     */
    @RequestMapping("/study_way")
    public String list(){
        return  "study_way";
    }

    /**
     * 游客分享表单页面
     * @return
     */
    @RequestMapping("share")
    public String share(){
        return "share";
    }

    /**
     * 关于页面
     * @return
     */
    @RequestMapping("about")
    public String about(){
        return "about";
    }

    @RequestMapping("list")
    public String articleList(){
        return "list";
    }

}
