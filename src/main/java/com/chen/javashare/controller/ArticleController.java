package com.chen.javashare.controller;

import com.chen.javashare.entity.Article;
import com.chen.javashare.entity.Category;
import com.chen.javashare.service.IArticleService;
import com.chen.javashare.vo.ArticleVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.sql.Date;
import java.util.*;

@RestController
public class ArticleController {

    @Autowired
    private IArticleService articleService;
    /**
     * 首页返回数据
     * @return
     */
    @RequestMapping(value = "index")
    public ModelAndView ArticleIndex(){
        // ---------------------- 调试数据 ----------------------

        // ---------------------- 调试数据 ----------------------


        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject("data",data());
        return modelAndView;
    }

    public static List<ArticleVo> data(){
        List<ArticleVo> vos = new ArrayList<>();
        for (int i = 1;i<6;i++){
            Category category = new Category();
            category.setId(i);
            category.setName("分类名称"+"----------"+i);
            category.setType(i);

            List<Article> list = new ArrayList<>();
            for (int j = 1;j<=10;j++){
                Article article = new Article();
                article.setTitle("名称我是名称是名称我是名称我是名称");
//                article.setCreateDate(new Date());
                article.setId(j);
                list.add(article);
            }
            ArticleVo articleVo = new ArticleVo();
            articleVo.setCategory(category);
            articleVo.setArticles(list);

            vos.add(articleVo);
        }

        return vos;
    }

    @RequestMapping(value = "/article/{id}")
    public ModelAndView articleInfo(@PathVariable(value = "id") String id){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("contitle","文章标题123");
        modelAndView.addObject("author","文章作者123");
        modelAndView.addObject("createdDate","创建时间123");
        modelAndView.addObject("type","Java123");
        modelAndView.setViewName("info");
        modelAndView.addObject("content","<p>博客从最初的域名购买，到上线已经有四年的时间了，这四年的时间，有笑过，有怨过，有悔过，有执着过，也有放弃过…但最后还是坚持了下来，时间如此匆匆，等再回过头已来不及去弥补…</p>\n" +
                "                    <p><strong>新增那些出色的功能：</strong></p>\n" +
                "                    <p><img src=\"http://www.yangqq.com/d/file/jstt/bj/2015-01-09/bb9e1b25215426d6c375e453c756f136.jpg\"></p>\n" +
                "                    <p>新功能：</p>\n" +
                "                    <p>1、支持4K高分辨率，在4K分辨率的显示器上可以获得佳效果。</p>\n" +
                "                    <p>2、PDF这个功能加强，CAD2018生成的PDF文件可以存储为几何对象。</p>\n" +
                "                    <p>3、视觉效果加强，优化填充和显示性能。</p>\n" +
                "                    <p>4、图形卡的反走样和高质量图形可以相互独立控制。</p>\n" +
                "                    <p>5、外部参照路径改为为&lt;相对&gt;，功能变得更强大。</p>\n" +
                "                    <p>6、更新DWG格式，打开和保存速度更快。</p>\n" +
                "                    <p>AutoCAD2018官方破解中文版64位安装教程：</p>\n" +
                "                    <p>1、下载AutoCAD2018官方破解中文版64位的软件，然后解压。</p>\n" +
                "                    <p>2、根据提示安装完成。</p>\n" +
                "                    <p>3、打开AutoCAD2018输入序列号和密钥（666-69696969,001J1）</p>\n" +
                "                    <p>4、打开注册机开始激活</p>\n" +
                "                    <p>5、把申请号粘贴到注册机里面生成激活码，把激活码粘贴到AutoCAD2018激活界面里面。</p>\n" +
                "                    <p>6、点击下一步，激活完成</p>");
        return modelAndView;
    }
}
