package com.chen.javashare;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@MapperScan("com.chen.javashare.mapper")//将项目中对应的mapper类的路径加进来就可以了
public class JavashareApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavashareApplication.class, args);
    }

}
